#!/usr/bin/python3

import http.client
import urllib

"""
    For the DVWA database to be initialized by the application,
    we need to query the page setup.php, recover the session cookie and the anti-CSRF token,
    and then send a POST request to the same page using those values.
"""
h = http.client.HTTPConnection('localhost', port=80)
# First request: get the form
h.request('GET', '/setup.php', None, {"Host": "localhost"})
r = h.getresponse()

# Retrieve the cookie
rh = r.getheaders()
cookie = next(header[1].split(';')[0] for header in rh if (header[0] == 'Set-Cookie' and header[1].startswith('PHPSESSID')))
print(cookie)

# Retrieve the token
body = r.read().decode("utf-8")
token = body[body.find("token' value='")+14:]
token = token[:token.find("'")]
print(token)

# Custom POST request
form_values = {'create_db': 'Create / Reset Database', 'user_token': token}
body = urllib.parse.urlencode(form_values)
headers = {'Host': 'localhost', 'Content-type': 'application/x-www-form-urlencoded','Cookie' : cookie}
h.request('POST', '/setup.php', body, headers)

# We should be ok!