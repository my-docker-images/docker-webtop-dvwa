#!/bin/bash

cp    -TRn /init-config/ /config
chown -R   abc:abc       /config

# MyDocker send a username to $1 and a password to $2
if [ "$2" ]; then
    export CUSTOM_USER=$1
    export PASSWORD=$2
fi

/etc/init.d/apache2 start
/etc/init.d/mariadb start

exec /init
