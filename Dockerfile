FROM gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-webtop:xfce

RUN                                          \
    echo "**** install dependencies ****" && \
    apt-get update                        && \
    apt-get install -y                       \
        apache2 mariadb-server mariadb-client php php-mysql php-gd libapache2-mod-php sqlmap

RUN                                                                     \
    cd /var/www/html                                                 && \
    curl -O -L https://github.com/digininja/DVWA/archive/master.zip  && \
    unzip master.zip                                                 && \
    mv DVWA-master dvwa                                              && \
    rm master.zip                                                    && \
    cp dvwa/config/config.inc.php.dist dvwa/config/config.inc.php    && \
    sed -i -e "s/$_DVWA\[ 'default_security_level' \] = 'impossible';/$_DVWA\[ 'default_security_level' \] = 'low';"/ \
              dvwa/config/config.inc.php                             && \
    sed -i -e 's/allow_url_fopen = On/allow_url_fopen = Off'/           \
              /etc/php/8.2/apache2/php.ini                           && \
    sed -i -e 's@DocumentRoot /var/www/html@DocumentRoot /var/www/html/dvwa'@ \
              /etc/apache2/sites-enabled/000-default.conf

COPY create_dvwa_database.txt /tmp/create_dvwa_database.txt
COPY setup_dvwa_database.py /tmp/setup_dvwa_database.py

RUN                                                 \
    /etc/init.d/mariadb start                    && \
    /etc/init.d/apache2 start                    && \
    sleep 1                                      && \
    mysql -u root </tmp/create_dvwa_database.txt && \
    python3 /tmp/setup_dvwa_database.py          && \
    /etc/init.d/apache2 stop                     && \
    /etc/init.d/mariadb stop

COPY burpsuite_install_rep.txt /tmp/burpsuite_install_rep.txt
RUN                                                      \
    cd /tmp                                           && \
    curl -L https://portswigger.net/burp/releases/startdownload\?product\=community\&version\=2024.8.5\&type\=Linux \
         -o burpsuite_community_linux_v2024_8_5.sh && \
    sh burpsuite_community_linux_v2024_8_5.sh -c <burpsuite_install_rep.txt

# Burp extension : CO2
RUN                                                                                   \
    curl -L -o /tmp/c5071c7a7e004f72ae485e8a72911afc.bapp \
         https://portswigger-cdn.net/bappstore/bapps/download/c5071c7a7e004f72ae485e8a72911afc/12 && \
    mkdir -p /init-config/.BurpSuite/bapps/c5071c7a7e004f72ae485e8a72911afc && \
    unzip /tmp/c5071c7a7e004f72ae485e8a72911afc.bapp \
          -d /init-config/.BurpSuite/bapps/c5071c7a7e004f72ae485e8a72911afc

## add modified wrapperscript
COPY /root /

RUN                            \
  echo "**** clean up ****" && \
  apt-get clean             && \
  rm -rf                       \
    /tmp/*                     \
    /var/lib/apt/lists/*       \
    /var/tmp/*

COPY files /init-config

