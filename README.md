# docker-webtop-dvwa
## Why

This image, based on [docker-webtop](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-webtop), 
provides the environment needed for Web application security lab at CentraleSupélec.

It uses the [Xfce](https://www.xfce.org/) desktop environment.

This image contains:
- An (Apache) web server and a database server (MySQL / MariaDB) used to run Damn Vulnerable
  Web Application ([DVWA](http://www.dvwa.co.uk/)), an educational web application
  illustrating many vulnerabilities and accessible from inside the container
  at http://localhost/;
- [Burp Suite Community Edition](https://portswigger.net/burp);
- The sqlmap command-line tool.

## Details

- See details on [docker-webtop](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-webtop/-/tree/xfce)
- if docker is installed on your computer, you can run (amd64 architecture only) this 
  image, assuming you are in a specific folder that will be shared with the container at 
  `/config`, with:
  
  `docker run -p 3000:3000 -v "$(pwd):/config"
    gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-webtop-dvwa:latest`


