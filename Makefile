
REPO = gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-webtop-dvwa
TAG  = latest

all:
	docker buildx create --use --node new-builder
	docker buildx build --push --platform "linux/amd64" --tag "${REPO}:${TAG}" .

